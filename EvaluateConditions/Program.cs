﻿using System;
using System.Linq;

namespace EvaluateConditions
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 6;
            int b = 4;
            int y = 6;
            int z = 2;

            bool result = a < (b - z) && (y > z);

            Console.ReadKey();

            string userInput = Console.ReadLine();

            Console.WriteLine(String.Join("", userInput.Reverse()));

            Console.WriteLine("Hello, " + Environment.UserName);
            Console.WriteLine("Press any key to exit.");
            Environment.Exit(0);
        }
    }
}
