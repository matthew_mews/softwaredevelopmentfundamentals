﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPCar
{
    class Car : ICar
    {
        public string Make { get; set; }
        public string Model { get; set; }

        public Car(string make, string model)
        {
            this.Make = make;
            this.Model = model;
        }

        public string Go() {
        
            return "Going";
        }

        public string Stop()
        {
            return "Stopping";
        }
    }
}
