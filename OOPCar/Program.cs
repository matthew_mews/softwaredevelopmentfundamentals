﻿using System;

namespace OOPCar
{
    class Program
    {
        static void Main(string[] args)
        {
            Car myCar = new Car("BMW", "M4");
            Car anotherCar = new Car("Aston Martin", "V8");

            Console.WriteLine(myCar.Make);
        }
    }
}
