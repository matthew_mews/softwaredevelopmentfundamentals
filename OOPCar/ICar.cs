﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPCar
{
    interface ICar
    {
        string Make { get; set; }
        string Model { get; set; }
        string Go();
        string Stop();
    }
}
