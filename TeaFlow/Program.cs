﻿using System;

namespace TeaFlow
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("How many sugars?");
            byte amountOfSugars = Convert.ToByte(Console.ReadLine());

            Console.WriteLine("Do you want milk?");
            bool milk = Convert.ToBoolean(Console.ReadLine());

            //bool milk = Console.ReadLine().ToLower() == "yes" ? true : false;

            Console.WriteLine("Is the kettle working?");
            bool kettleWorking = Convert.ToBoolean(Console.ReadLine());

            Console.WriteLine("Is the mug clean?");
            bool mugIsClean = Convert.ToBoolean(Console.ReadLine());

            // create an array of teabags
            string[] teaBags = { "Builders", "Raspberry", "Erl Grey" };

            Console.WriteLine("What kind of tea would you like? Please enter the number of your chosen teabag.");

            for (int i = 0; i < teaBags.Length; i++)
            {
                Console.WriteLine(teaBags[i]);
            }

            Console.WriteLine("What kind of tea would you like?");
            string teaBag = Console.ReadLine();

            Console.WriteLine("Do you want to stir the teabag?");
            bool stirTeaBag = Convert.ToBoolean(Console.ReadLine());

            Console.WriteLine("Who is making the tea?");
            string teaMaker = Console.ReadLine();

            // Check if the kettle is working
            if (kettleWorking)
            {
                Console.WriteLine("Boil the kettle");

                if (mugIsClean)
                {
                    Console.WriteLine("Mug is clean");

                    // Add teabag
                    Console.WriteLine($"Tea bag is in {teaBag}, Who is making tea {teaMaker}, Was the mug clean {mugIsClean}");
                    Console.WriteLine($"Did {teaMaker} stir the teabag {stirTeaBag}");
                } else
                {
                    Console.WriteLine("The mug is not clean");
                }
            }
            else
            {
                Console.WriteLine("The kettle is not working");
            }
        }
    }
}
