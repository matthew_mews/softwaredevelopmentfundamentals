﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapeOOP
{
    class Rectangle : Shape
    {
        public Rectangle(double length, double width)
        {
            this.Length = length;
            this.Width = width;
        }
    }
}
