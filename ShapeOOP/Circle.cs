﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapeOOP
{
    class Circle : Shape
    {
        public Circle(double length, double width)
        {
            this.Length = length;
            this.Width = width;
        }
    }
}
