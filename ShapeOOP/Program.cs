﻿using System;

namespace ShapeOOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rec = new Rectangle(1.4, 3.7);
            Circle circle = new Circle(1.7, 4.7);

            Console.WriteLine(rec.CalculateArea());
            Console.WriteLine(circle.CalculateArea());
        }
    }
}
