﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapeOOP
{
    abstract class Shape : IShape
    {
        public double Length { get; set; }
        public double Width { get; set; }
        public double CalculateArea()
        {
            return this.Length * this.Width;
        }
    }
}
