﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShapeOOP
{
    interface IShape
    {
        double Length { get; set; }
        double Width { get; set; }
        double CalculateArea();
    }
}
